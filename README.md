# **Docker Analytics**

Central repository for the Docker Analytics application. Make [Docker Analytics Server](https://gitlab.com/flow.gunso/docker-analytics-server/) and [Docker Analytics Client](https://gitlab.com/flow.gunso/docker-analytics-client/) available into a single Docker Registry endpoint.

There's a live instance available at [docker-seafile-client.analytics.flwgns.ovh](https://docker-seafile-client.analytics.flwgns.ovh/) that tracks pulls and stars count for [flowgunso/seafile-client](https://hub.docker.com/r/flowgunso/seafile-client). 

## Supported tags
[`api`](registry.gitlab.com/flow.gunso/docker-analytics:api),
[`api-0`](registry.gitlab.com/flow.gunso/docker-analytics:api-0),
[`api-0.2`](registry.gitlab.com/flow.gunso/docker-analytics:api-0.2),
[`api-0.2.0`](registry.gitlab.com/flow.gunso/docker-analytics:api-0.2.0) the API container, make the data available over HTTP.  
[`worker`](registry.gitlab.com/flow.gunso/docker-analytics:worker),
[`worker-0`](registry.gitlab.com/flow.gunso/docker-analytics:worker-0),
[`worker-0.2`](registry.gitlab.com/flow.gunso/docker-analytics:worker-0.2),
[`worker-0.2.0`](registry.gitlab.com/flow.gunso/docker-analytics:worker-0.2.0) the worker container, periodically fetch the data from the Docker API.  
[`client`](registry.gitlab.com/flow.gunso/docker-analytics:client),
[`client0`](registry.gitlab.com/flow.gunso/docker-analytics:client-0),
[`client0.2`](registry.gitlab.com/flow.gunso/docker-analytics:client-0.2),
[`client0.2.0`](registry.gitlab.com/flow.gunso/docker-analytics:client-0.2.0) the client container, it is the web interface for users.  
