#!/bin/bash

git clone https://gitlab.com/flow.gunso/docker-analytics-server.git server/
cd server/

targets=("api" "worker")
for target in "${targets[@]}"; do
    docker build --build-arg BUILD_TARGET=$target --file .ci/Dockerfile --tag $CI_PROJECT_NAME:$target .
    docker save --output ../$CI_PROJECT_NAME.$target.tar $CI_PROJECT_NAME:$target
done
