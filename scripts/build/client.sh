#!/bin/bash

git clone https://gitlab.com/flow.gunso/docker-analytics-client.git client/
cd client/

docker build --file .ci/Dockerfile --tag $CI_PROJECT_NAME:client .
docker save --output ../$CI_PROJECT_NAME.client.tar $CI_PROJECT_NAME:client
