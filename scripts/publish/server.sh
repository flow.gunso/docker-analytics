#!/bin/bash

# Validate the tag to the SemVer.
semver_regex="^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"
regex_validation=$(echo $CI_COMMIT_TAG | grep -P $semver_regex)
if [ -z $regex_validation ]; then
    echo "Version tag $CI_COMMIT_TAG does not adhere to SemVer!"
    exit 1
fi

# Generate the tag version list with major, minor and revision number.
tags=("latest")
tag=""
for version_component in $(echo $CI_COMMIT_TAG | tr "." "\n"); do
    tag+="$version_component"
    tags+=("$tag")
    tag+="."
done

# Push tagged images.
targets=("api" "worker")
docker login --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD $CI_REGISTRY
for target in "${targets[@]}"; do
    docker load --input $CI_PROJECT_NAME.$target.tar
    for tag in "${tags[@]}"; do
        docker tag $CI_PROJECT_NAME:$target $CI_REGISTRY_IMAGE:$target-$tag
        docker push $CI_REGISTRY_IMAGE:$target-$tag
    done
done
